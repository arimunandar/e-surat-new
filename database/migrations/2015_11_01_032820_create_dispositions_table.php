<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->string('status')->default('0');
            $table->integer('inbox_id')->unsigned();
            $table->timestamps();
            
            $table->foreign('inbox_id')
                    ->references('id')
                    ->on('inboxes')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dispositions');
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Admin',
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'admin'
        ]);
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Operator',
            'username' => 'operator',
            'email' => 'operator@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'operator'
        ]);
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Ary Munandar',
            'username' => 'kk',
            'email' => 'dictatorkid@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'kepala kantor'
        ]);
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Sudin P A',
            'username' => 'kb',
            'email' => 'dvan1005@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'kepala bagian'
        ]);
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Otong',
            'username' => 'skk',
            'email' => 'skk@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'staff kepala kantor'
        ]);
        DB::table('users')->insert([
            'no_induk' =>  str_random(10),
            'name' => 'Samsul',
            'username' => 'skb',
            'email' => 'skb@gmail.com',
            'password' => bcrypt('vandal421'),
            'level' => 'staff kepala bagian'
        ]);
    }
}

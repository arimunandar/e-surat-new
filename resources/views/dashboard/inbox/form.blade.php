@extends('app')

@section('htmlheader_title')
    Manajement Surat Masuk
@endsection


@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Form Data Surat Masuk</h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Warning !</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(array('role' => 'form', 'url' => 'inbox')) !!}
                            {!! Form::hidden('id', !empty($model->id) ? $model->id : '') !!}
                            <div class="form-group">
                                <label>Kode Surat</label>
                                {!! Form::text('nomor', !empty($model->nomor) ? $model->nomor : $nomor, array('class' => 'form-control', 'placeholder' => 'Nomor Surat Masuk', 'readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Kategori</label>
                                {!! Form::select('kategori', $kategori, !empty($model->kategori) ? $model->kategori : $kategori, array('class' => 'form-control', 'data-placeholder' => 'Select Kategori')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tanggal Surat Masuk</label>                                
                                {!! Form::text('tanggal', !empty($model->tanggal) ? $model->tanggal : '', array('class' => 'form-control', 'placeholder' => 'Tanggal Surat Masuk', 'id' => 'date')) !!}
                            </div>
                        </div>                            
                            <div class="form-group col-md-3">
                                <label>Tanggal Surat</label>                                
                                {!! Form::text('tanggal_surat', !empty($model->tanggal_surat) ? $model->tanggal_surat : '', array('class' => 'form-control', 'placeholder' => 'Tanggal / Nomor Surat', 'id' => 'date1')) !!}
                            </div>
                            <div class="form-group col-md-3">
                                <label>Nomor Surat</label>                                
                                {!! Form::text('no_surat', !empty($model->no_surat) ? $model->no_surat : '', array('class' => 'form-control', 'placeholder' => 'Nomor Surat')) !!}
                            </div>
                            <div class="form-group col-md-6">
                                <label>Asal Surat</label>                                
                                {!! Form::text('asal', !empty($model->asal) ? $model->asal : '', array('class' => 'form-control', 'placeholder' => 'Asal Surat')) !!}
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Perihal Surat</label>
                                    {!! Form::text('perihal', !empty($model->perihal) ? $model->perihal : '', array('class' => 'form-control', 'placeholder' => 'Perihal Surat')) !!}
                                </div>                        
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Pilih User</label>
                                    {!! Form::select('user[]', $user, !empty($model->user->user_id) ? $model->user->user_id : '', 
                                        array(
                                            'id' => 'e2', 
                                            'class' => 'form-control select2', 
                                            'multiple' => 'multiple', 
                                            'data-placeholder' => 
                                            'Pilih User')) 
                                    !!}
                                </div>
                            </div>
                            <div class="col-md-12"> 
                                <hr>                           
                                <a href="{{ url('inbox') }}" class="btn btn-info" title="Back !">
                                    <i class="fa fa-reply"></i> Back
                                </a>
                                <button type="submit" class="btn bg-purple" title="Save Data !">
                                    <i class="fa fa-save"></i> Save
                                </button>
                            </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop

@extends('app')

@section('htmlheader_title')
    Home
@endsection


@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="callout callout-info">
        <h4>Tip!</h4>
        <p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar is bigger than your content because it prevents extra unwanted scrolling.</p>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title"><b>Data Table With Full Features</b></h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        <table class="table table-no-bordered table-striped">
                            <thead>
                                <tr>
                                    <td width="30px"><i class="fa fa-bug"></i></td>
                                    <td width="200px"><b>Kode Surat</b></td>
                                    <td>{{ $inbox->nomor }}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-life-ring"></i></td>
                                    <td><b>Kategori</b></td>
                                    <td>{{ strtoupper($inbox->kategori) }}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-calendar-o"></i></td>
                                    <td><b>Tanggal Surat Keluar</b></td>
                                    <td>{{ $inbox->tanggal }}</td>
                                </tr>
                                <tr>
                                    <td width="30px"><i class="fa fa-calendar"></i></td>
                                    <td width="150px"><b>Tanggal / Nomor Surat</b></td>
                                    <td>{{ strtoupper($inbox->tanggal_surat. '-' .$inbox->no_surat) }}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa  fa-odnoklassniki"></i></td>
                                    <td><b>Perihal</b></td>
                                    <td>{{ ucwords($inbox->perihal) }}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-road"></i></td>
                                    <td><b>Alamat Asal Surat</b></td>
                                    <td>{{ ucwords($inbox->asal) }}</td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-warning"></i></td>
                                    <td><b>Status</b></td>
                                    <td>
                                        @if ($inbox->status == 0)
                                            Belum Di Disposisi
                                        @elseif ($inbox->status == 1)
                                            Sudah Di Disposisi
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td><i class="fa fa-send-o"></i></td>
                                    <td><b>Diterima</b></td>
                                    <td>{{ $inbox->created_at->diffForHumans() }}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <hr>
                    <div class="col-md-6 pull-left">
                        <a href="{{ url('inbox') }}" data-toggle="tooltip" title="Back !" class="btn btn-info"><i class="fa fa-reply"></i> Back</a>
                        <a href="{{ url('inbox/' .$inbox->id. '/edit') }}" data-toggle="tooltip" title="Edit !" class="btn bg-purple"><i class="fa fa-edit"></i> Edit</a>
                        <a href="{{ url('disposition/create/' .$inbox->id) }}" data-toggle="tooltip" title="Disposition !" class="btn bg-orange"><i class="fa fa-forward"></i> Disposition</a>
                    </div>
                    <div class="col-md-6 pull-right text-right">
                        {!! Form::open(['method' => 'DELETE', 'url' => 'inbox/' .$inbox->id]) !!}
                            <button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Delete !"><i class="fa fa-trash-o"></i> Delete</button>
                        {!! Form::close() !!}
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection

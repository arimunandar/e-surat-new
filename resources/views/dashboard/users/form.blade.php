@extends('app')

@section('htmlheader_title')
    Company
@endsection


@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header bg-info">
                    <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    {!! Form::open(array('role' => 'form', 'url' => 'company')) !!}
                        {!! Form::hidden('id', !empty($model->id) ? $model->id : '') !!}
                        <div class="form-group">
                            <label>Company Name</label>
                            {!! Form::text('name', !empty($model->name) ? $model->name : '', array('class' => 'form-control', 'placeholder' => 'Company Name')) !!}
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            {!! Form::textarea('address', !empty($model->address) ? $model->address : '',array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            {!! Form::number('telp', !empty($model->telp) ? $model->telp : '', array('class' => 'form-control')) !!}
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            {!! Form::text('email', !empty($model->email) ? $model->email : '', array('class' => 'form-control', 'placeholder' => 'Email'))!!}
                        </div>
                        <hr>
                        <a href="{{ url('company') }}" type="submit" class="btn btn-danger">
                            <i class="fa fa-reply"></i> Cancel
                        </a>
                        <button type="submit" class="btn btn-info">
                        <i class="fa fa-save"></i> Save
                        </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@stop
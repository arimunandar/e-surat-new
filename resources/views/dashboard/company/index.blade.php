@extends('app')

@section('htmlheader_title')
    Home
@endsection


@section('main-content')
<!-- Main content -->
<section class="content">
    
    <div class="callout callout-info">
        <h4>Tip!</h4>
        <p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar is bigger than your content because it prevents extra unwanted scrolling.</p>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Data Table With Full Features</h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive">
                    <div class="col-md-12">
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p>{{ session('success') }}</p>
                            </div>
                        @endif
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; ?>
                                @foreach($company as $c)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$c->name}}</td>
                                        <td>{{$c->address}}</td>
                                        <td>{{$c->telp}}</td>
                                        <td>{{$c->email}}</td>
                                        <td>
                                            <div class="text-center">
                                                <a href="{{ url('company/'.$c->id) }}" data-toggle="tooltip" title="Show Detail !" class="btn bg-navy"><i class="fa fa-eye"></i></a>                                     
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $no++ ?>
                                @endforeach
                            </tbody>
                        </table>
                        <a href="{{ url('company/create') }}" data-toggle="tooltip" title="Add New Data!" class="btn bg-navy"><i class="fa fa-plus"></i> Add New Data</a>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection

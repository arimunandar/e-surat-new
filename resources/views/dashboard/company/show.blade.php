@extends('app')

@section('htmlheader_title')
    Home
@endsection


@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="callout callout-info">
        <h4>Tip!</h4>
        <p>Add the fixed class to the body tag to get this layout. The fixed layout is your best option if your sidebar is bigger than your content because it prevents extra unwanted scrolling.</p>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="col-md-12">
                    	<h3 class="box-title"><b>Data Table With Full Features</b></h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                	<div class="col-md-12">
                		<table class="table table-no-bordered table-striped">
	                	    <thead>
	                	        <tr>
	                	        	<td width="30px"><i class="fa fa-bank"></i></td>
	                	            <td width="150px"><b>Company Name</b></td>
	                	            <td>{{ $company->name }}</td>
	                	        </tr>
	                	        <tr>
	                	        	<td><i class="fa fa-road"></i></td>
	                	            <td><b>Company Address</b></td>
	                	            <td>{{ $company->address }}</td>
	                	        </tr>
	                	        <tr>
	                	        	<td><i class="fa fa-phone"></i></td>
	                	            <td><b>Phone</b></td>
	                	            <td>{{ $company->telp }}</td>
	                	        </tr>
	                	        <tr>
	                	        	<td><i class="fa fa-envelope"></i></td>
	                	            <td><b>Email</b></td>
	                	            <td>{{ $company->email }}</td>
	                	        </tr>
	                	    </thead>
	                	</table>
                	</div>
                	<hr>
                	<div class="col-md-6 pull-left">
                		<a href="{{ url('company') }}" data-toggle="tooltip" title="Back !" class="btn btn-info"><i class="fa fa-reply"></i> Back</a>
                    	<a href="{{ url('company/' .$company->id. '/edit') }}" data-toggle="tooltip" title="Edit !" class="btn bg-purple"><i class="fa fa-edit"></i> Edit</a>
                	</div>
                	<div class="col-md-6 pull-right text-right">
                		{!! Form::open(['method' => 'DELETE', 'url' => 'company/' .$company->id]) !!}
							<button type="submit" class="btn btn-danger" data-toggle="tooltip" title="Delete !"><i class="fa fa-trash-o"></i> Delete</button>
						{!! Form::close() !!}
                	</div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@endsection

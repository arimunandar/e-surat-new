@extends('app')

@section('htmlheader_title')
    Manajemen Perusahaan
@endsection


@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Form Data Perusahaan</h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-6">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Warning !</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(array('role' => 'form', 'url' => 'company')) !!}
                            {!! Form::hidden('id', !empty($model->id) ? $model->id : '') !!}
                            <div class="form-group">
                                <label>Company Name</label>
                                {!! Form::text('name', !empty($model->name) ? $model->name : '', array('class' => 'form-control', 'placeholder' => 'Company Name')) !!}
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                {!! Form::textarea('address', !empty($model->address) ? $model->address : '',array('class' => 'form-control', 'placeholder' => 'Company Address')) !!}
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                {!! Form::number('telp', !empty($model->telp) ? $model->telp : '', array('class' => 'form-control', 'placeholder' => 'Phone Number')) !!}
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                {!! Form::text('email', !empty($model->email) ? $model->email : '', array('class' => 'form-control', 'placeholder' => 'Email'))!!}
                            </div>
                            <hr>
                            <a href="{{ url('company') }}" class="btn btn-info" title="Back !">
                                <i class="fa fa-reply"></i> Back
                            </a>
                            <button type="submit" class="btn bg-purple" title="Save Data !">
                            <i class="fa fa-save"></i> Save
                            </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
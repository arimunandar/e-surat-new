@extends('app')

@section('htmlheader_title')
    Manajement Surat Disposisi
@endsection


@section('main-content')
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header">
                    <div class="col-md-12">
                        <h3 class="box-title">Form Data Surat Masuk</h3><hr>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="col-md-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-ban"></i> Warning !</h4>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(array('role' => 'form', 'url' => 'disposition')) !!}
                            {!! Form::hidden('inbox_id', !empty($model->id) ? $model->id : '') !!}
                            <div class="form-group">
                                <label>Kode Surat</label>
                                {!! Form::text('nomor', !empty($model->nomor) ? $model->nomor : $nomor, array('class' => 'form-control', 'placeholder' => 'Nomor Surat Masuk', 'readonly')) !!}
                            </div>
                            <div class="form-group">
                                <label>Disposisi</label>
                                {!! Form::textarea('description', !empty($model->description) ? $model->description : '', array('class' => 'form-control', 'placeholder' => 'Disposisi')) !!}
                            </div> 
                            <div class="form-group">
                                <label>Pilih User</label>
                                {!! Form::select('user[]', $user, null, 
                                    array(
                                        'id' => 'e2', 
                                        'class' => 'form-control select2', 
                                        'multiple' => 'multiple', 
                                        'data-placeholder' => 
                                        'Pilih User')) 
                                !!}
                            </div>
                            <hr>                           
                            <a href="{{ url('inbox') }}" class="btn btn-info" title="Back !">
                                <i class="fa fa-reply"></i> Back
                            </a>
                            <button type="submit" class="btn bg-purple" title="Save Data !">
                                <i class="fa fa-save"></i> Save
                            </button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

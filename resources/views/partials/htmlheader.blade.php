<head>
    <meta charset="UTF-8">
    <title> E-Surat - @yield('htmlheader_title', 'E-Surat') </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="textassets/css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <!-- Theme style -->
    @yield('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/css/sweetalert.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/date.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/AdminLTE.css') }}" rel="stylesheet" type="textassets/css" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/select2-bootstrap.css') }}">
    <link href="{{ asset('assets/css/skins/_all-skins.min.css') }}" rel="stylesheet" type="textassets/css" />
    <!-- iCheck -->
    <link href="{{ asset('assets/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="textassets/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

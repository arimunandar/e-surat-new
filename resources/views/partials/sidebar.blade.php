<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('assets/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ Auth::user()->email. ' | ' .strtoupper(Auth::user()->level) }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="{{ url('dashboard') }}"><i class='fa fa-home'></i> <span>Beranda</span></a></li>
            <li><a href="{{url('company')}}"><i class='fa fa-flag'></i> <span>Perusahaan</span></a></li>
            <li><a href="{{url('users')}}"><i class='fa fa-users'></i> <span>Users</span></a></li>
            <li class="treeview">
                <a href="#"><i class='fa fa-file'></i> <span>Surat</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('inbox')}}">Surat Masuk</a></li>
                    <li><a href="{{url('outbox')}}">Surat Keluar</a></li>
                    <li><a href="{{url('index-disposition')}}">Kotak Masuk Disposisi</a></li>
                    <li><a href="{{url('outbox-disposition')}}">Kotak Keluar Disposisi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class='fa fa-print'></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{url('report-inbox')}}">Surat Masuk</a></li>
                    <li><a href="{{url('report-outbox')}}">Surat Keluar</a></li>
                    <li><a href="{{url('report-disposition')}}">Surat Disposisi</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
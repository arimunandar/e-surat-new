<!-- REQUIRED JS SCRIPTS -->
<!-- jQuery 2.1.4 -->
<script src="{{ asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>

@yield('script')
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/sweetalert-dev.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/date.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/plugins/fastclick/fastclick.min.js') }}"></script>
<script src="{{ asset('assets/plugins/select2/select2.js') }}"></script>
<!-- page script -->
<script>

	$(function () {
		$(".select2").select2();
		$("#example1").DataTable();
		$("#example2").DataTable();
		$("#example3").DataTable();
		$("#example4").DataTable();
		$("#example5").DataTable();
	});
	// When the document is ready
    $(document).ready(function () {
        
        $('#date').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });  
        $('#date1').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    
    });
</script>
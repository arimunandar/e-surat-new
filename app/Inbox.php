<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model
{
    protected $table = 'inboxes';

	protected $fillable = [
		'nomor',
		'kategori',
		'tanggal',
		'tanggal_surat',
		'no_surat',
		'asal',
		'perihal',
		'status'
	];

	public function user()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

     public function getUserListAttribute()
    {
    	return $this->users->lists('id');
    }

    public function disposition()
    {
		return $this->hasMany('App\disposition');
	}

	public function file()
	{
		return $this->hasMany('App\File');
	}
   
}

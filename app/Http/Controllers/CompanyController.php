<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Input;
use Validator;

class CompanyController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::all();
        return view('dashboard.company.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::lists('username', 'id');
        return view('dashboard.company.form', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $input = Input::except('_token');

        $validate = Validator::make($input, array(
            'name'    => 'required',
            'address' => 'required',
            'telp'    => 'required',
            'email'   => 'required|email',
        ));

        if (!$validate->fails()) {

            $model          = Company::firstOrNew(array('id' => $input['id']));
            $model->name    = $input['name'];
            $model->address = $input['address'];
            $model->telp    = $input['telp'];
            $model->email   = $input['email'];
            $model->save();

            return redirect(route('company.index'))->with('success', 'Add New Data Success !!');
        }

        return redirect()->back()->withErrors($validate->messages());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('dashboard.company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Company::Find($id);

        return view('dashboard.company.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::where('id', $id)->first();

        $company->delete();

        return redirect(route('company.index'))->with('success', 'Data berhasil di hapus!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Outbox;
use Illuminate\Http\Request;
use Input;
use Validator;

class OutboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $outbox = Outbox::orderBy('created_at', 'DESC')->get();
        return view('dashboard.outbox.index', compact('outbox'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

        $date     = date('Y') . '-SK-';
        $karakter = 'ABCDEFGHIJKL1234567890';
        $string   = '';
        for ($i = 0; $i < 6; $i++) {
            $pos = rand(0, strlen($karakter) - 1);
            $string .= $karakter{$pos};
        }
        $nomor    = $date . $string;
        $kategori = array(
            ''      => '--- Pilih Kategori ---',
            'biasa'   => 'BIASA',
            'penting' => 'PENTING',
            'rahasia' => 'RAHASIA',
        );
        return view('dashboard.outbox.form', compact('nomor', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::except('_token');

        $validate = Validator::make($input, array(
            'nomor'         => 'required',
            'judul'         => 'required',
            'kategori'      => 'required',
            'tanggal'       => 'required',
            'tanggal_surat' => 'required',
            'no_surat'      => 'required',
            'perihal'       => 'required',
            'tujuan'        => 'required',
        ));

        if (!$validate->fails()) {

            $model                = Outbox::firstOrNew(array('id' => $input['id']));
            $model->nomor         = $input['nomor'];
            $model->judul         = $input['judul'];
            $model->kategori      = $input['kategori'];
            $model->tanggal       = $input['tanggal'];
            $model->tanggal_surat = $input['tanggal_surat'];
            $model->no_surat      = $input['no_surat'];
            $model->perihal       = $input['perihal'];
            $model->tujuan        = $input['tujuan'];
            $model->save();

            return redirect(route('outbox.index'))->with('success', 'Add New Data Success !!');
        }

        return redirect()->back()->withErrors($validate->messages());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $outbox = Outbox::find($id);
        return view('dashboard.outbox.show', compact('outbox'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Outbox::Find($id);
        $kategori = array(
            'biasa'   => 'BIASA',
            'penting' => 'PENTING',
            'rahasia' => 'RAHASIA',
        );
        return view('dashboard.outbox.form', compact('model', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $outbox = Outbox::where('id', $id)->first();

        $outbox->delete();

        return redirect(route('outbox.index'))->with('success', 'Data berhasil di hapus!');
    }
}

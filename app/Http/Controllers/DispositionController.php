<?php

namespace App\Http\Controllers;

use App\Disposition;
use App\DispositionUser;
use App\Http\Controllers\Controller;
use App\Inbox;
use App\User;
use Illuminate\Http\Request;

class DispositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Disposition::all();

        return view('dashboard.disposition.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $user        = User::lists('name', 'id');
        $model = Inbox::find($id);
        return view('dashboard.disposition.form', compact('user', 'model'));
    }

    public function createDisposition($id)
    {
        $user        = User::lists('name', 'id');
        $model = Disposition::with(['inbox'])->findOrFail($id);
        return view('dashboard.disposition.form-disposition', compact('user', 'model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $disposition           = new Disposition();
        $disposition->description     = $request->input('description');
        $disposition->inbox_id = $request->input('inbox_id');
        $disposition->save();

        for ($i = 0; $i < count($request->input('user')); $i++) {
            $dispositionuser                 = new DispositionUser();
            $dispositionuser->user_id        = $request->input('user')[$i];
            $dispositionuser->disposition_id = $disposition->id;
            $dispositionuser->save();
        }

        return redirect(route('disposition.index'))->with('success', 'Data berhasil di kirim!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Disposition::with(['inbox'])->findOrFail($id);

        return view('dashboard.disposition.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

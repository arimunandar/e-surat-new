<?php

namespace App\Http\Controllers;

use App\File;
use App\Http\Controllers\Controller;
use App\Inbox;
use App\InboxUser;
use App\User;
use Illuminate\Http\Request;
use Input;
use Validator;

class InboxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inbox = Inbox::orderBy('created_at', 'DESC')->get();
        return view('dashboard.inbox.index', compact('inbox'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user     = User::lists('name', 'id');
        $date     = date('Y') . '-SK-';
        $karakter = 'ABCDEFGHIJKL1234567890';
        $string   = '';
        for ($i = 0; $i < 6; $i++) {
            $pos = rand(0, strlen($karakter) - 1);
            $string .= $karakter{$pos};
        }
        $nomor    = $date . $string;
        $kategori = array(
            ''        => '--- Pilih Kategori ---',
            'biasa'   => 'BIASA',
            'penting' => 'PENTING',
            'rahasia' => 'RAHASIA',
        );
        return view('dashboard.inbox.form', compact('user', 'nomor', 'kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::except('_token');

        $validate = Validator::make($input, array(
            'nomor'         => 'required',
            'kategori'      => 'required',
            'tanggal'       => 'required',
            'tanggal_surat' => 'required',
            'no_surat'      => 'required',
            'perihal'       => 'required',
            'asal'          => 'required',
            'user'          => 'required',
        ));

        if (!$validate->fails()) {

            $model                = Inbox::firstOrNew(array('id' => $input['id']));
            $model->nomor         = $input['nomor'];
            $model->kategori      = $input['kategori'];
            $model->tanggal       = $input['tanggal'];
            $model->tanggal_surat = $input['tanggal_surat'];
            $model->no_surat      = $input['no_surat'];
            $model->perihal       = $input['perihal'];
            $model->asal          = $input['asal'];
            $model->save();

            for ($i = 0; $i < count($request->input('user')); $i++) {
                $inboxuser           = new InboxUser();
                $inboxuser->user_id  = $request->input('user')[$i];
                $inboxuser->inbox_id = $model->id;
                $inboxuser->save();
            }

            return redirect(route('inbox.index'))->with('success', 'Add New Data Success !!');
        }

        return redirect()->back()->withErrors($validate->messages());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user     = User::lists('name', 'id');
        $inbox = Inbox::find($id);
        return view('dashboard.inbox.show', compact('inbox', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user     = User::lists('name', 'id');
        $model    = Inbox::with(['user'])->findOrFail($id);
        $kategori = array(
            ''        => '--- Pilih Kategori ---',
            'biasa'   => 'BIASA',
            'penting' => 'PENTING',
            'rahasia' => 'RAHASIA',
        );
        // $test = $model->user->toArray();
        dd($model->user);
        return view('dashboard.inbox.form', compact('model', 'kategori', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

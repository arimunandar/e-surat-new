<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
Route::get('/test', function () {
    return view('welcome');
});
Route::get('/', function () {
    return redirect('auth/login');
});
// Users
Route::resource('users', 'UserController');
// Auth
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
// Dashboard
Route::resource('dashboard', 'DashboardController');
// Inbox
Route::resource('inbox', 'InboxController');
//Disposition
Route::resource('disposition', 'DispositionController');
Route::get('disposition/create/{id}', 'DispositionController@create');
Route::get('disposition-create/{id}', 'DispositionController@createDisposition');
// Outbox
Route::resource('outbox', 'OutboxController');
// Company
Route::resource('company', 'CompanyController');

// Contoh Kirim Email
Route::get('email', function () {

    // Variable data ini yang berupa array ini akan bisa diakses di dalam "view".
    $data = ['prize' => 'Peke', 'total' => 3];

    // "emails.hello" adalah nama view.
    Mail::send('emails.index', $data, function ($mail) {
        $mail->to('arimunandar.dev@gmail.com', 'Ary Munandar');
        $mail->subject('Hello World!');
    });

});

Route::get('pdf', 'DashboardController@pdf');

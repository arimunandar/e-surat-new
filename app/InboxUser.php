<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InboxUser extends Model
{
    protected $table = 'inbox_user';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outbox extends Model
{
    protected $table = 'outboxes';

    protected $fillable = [
    	'nomor',
    	'judul',
    	'kategori',
    	'tanggal',
    	'tanggal_surat',
    	'no_surat',
    	'perihal',
    	'tujuan'
    ];
}

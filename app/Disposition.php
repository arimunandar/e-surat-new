<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disposition extends Model
{
	protected $table = 'dispositions';

    public function inbox()
    {
		return $this->belongsTo('App\Inbox');
	}

	public function user()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }
}
